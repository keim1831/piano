package application.logic;

import java.util.ArrayList;
import java.util.Collections;

public class CheckAccordance {
		
		public boolean check(ArrayList<String> listOne,ArrayList<String> listTwo) {			    
	        Collections.sort(listOne);
	        Collections.sort(listTwo);	         	         
	        return listOne.equals(listTwo) ? true : false;       
		}
}
