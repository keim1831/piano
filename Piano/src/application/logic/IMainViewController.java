package application.logic;

import application.ui.Router;

public interface IMainViewController {
	void setRouter(Router router);
	void setState(State state);
}
