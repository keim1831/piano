package application.logic;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

class PlaySequenceInfo {
	public Iterator<String> iterator;
	public boolean shouldContinue;
}

public class NotePlayer {
	private final HashMap<String, MediaPlayer> noteLookup;
	private PlaySequenceInfo currentlyPlaying;
    public static ArrayList<String> list = new ArrayList<String>();

	
	public NotePlayer() {
		noteLookup = new HashMap<String, MediaPlayer>();
		noteLookup.put("C4", createMediaPlayer("C4.mp3"));
		noteLookup.put("C4#", createMediaPlayer("C#4.mp3"));
		noteLookup.put("D4", createMediaPlayer("D4.mp3"));
		noteLookup.put("D4#", createMediaPlayer("D#4.mp3"));
		noteLookup.put("E4", createMediaPlayer("E4.mp3"));
		noteLookup.put("F4", createMediaPlayer("F4.wav")); // FIXME!
		noteLookup.put("F4#", createMediaPlayer("F#4.mp3"));
		noteLookup.put("G4", createMediaPlayer("G4.mp3"));
		noteLookup.put("G4#", createMediaPlayer("G#4.mp3"));
		noteLookup.put("A4", createMediaPlayer("A4.mp3"));
		noteLookup.put("A4#", createMediaPlayer("A#4.mp3"));
		noteLookup.put("B4", createMediaPlayer("B4.mp3"));
		noteLookup.put("C5", createMediaPlayer("C5.mp3"));
		noteLookup.put("C5#", createMediaPlayer("C#5.mp3"));
		noteLookup.put("D5", createMediaPlayer("D5.mp3"));
		noteLookup.put("D5#", createMediaPlayer("D#5.mp3"));
		noteLookup.put("E5", createMediaPlayer("E5.mp3"));
		noteLookup.put("pause", createMediaPlayer("p5.mp3"));
	}
	
	private MediaPlayer createMediaPlayer(String fileName) {
		String path = "resources\\samples\\" + fileName;  
        Media media = new Media(new File(path).toURI().toString());    
        return new MediaPlayer(media);  
	}
	
	public void playSingle(String keyLabel) {  
        var player = noteLookup.get(keyLabel);
        player.play();
        player.seek(new Duration(0));
	}
	
	public void playSequence(Iterator<String> keys)
	{
		stopSequence();
		currentlyPlaying = new PlaySequenceInfo(); 
		currentlyPlaying.shouldContinue = true;
		currentlyPlaying.iterator = keys;
		playRecursive(currentlyPlaying);
	}
	
	public void stopSequence() {
		if (currentlyPlaying != null) {
			currentlyPlaying.shouldContinue = false;
			currentlyPlaying = null;	
		}
	}
	
	public boolean isPlaying() {
		return currentlyPlaying != null && currentlyPlaying.iterator.hasNext();
	}
	
	private void playRecursive(PlaySequenceInfo currentlyPlaying)
	{
		if (!currentlyPlaying.shouldContinue)
			return;
	
		var current = currentlyPlaying.iterator.next();
		if (current.length() == 0) {
			var player = noteLookup.get("pause");
			list.add("");

	        player.play();
	        player.setOnEndOfMedia(new Runnable() {
	            @Override
	            public void run() {
	                player.stop();
	                if (currentlyPlaying.iterator.hasNext()) {
	                    playRecursive(currentlyPlaying);
	                }
	                return;
	            }
	        });	
		}
		else {
			var player = noteLookup.get(current);
			list.add(current);
			if (player == null) return;
	        player.play();
	        player.setOnEndOfMedia(new Runnable() {
	            @Override
	            public void run() {
	                player.stop();
	                if (currentlyPlaying.iterator.hasNext()) {
	                    playRecursive(currentlyPlaying);
	                }
	                return;
	            }
	        });
		}
	}
}
