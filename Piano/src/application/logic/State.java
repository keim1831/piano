package application.logic;

import java.util.ArrayList;

public class State {
	private ArrayList<Integer> scores;

	public State() {
		scores = new ArrayList<Integer>();
	}
	
	public ArrayList<Integer> getScores() {
		return scores;
	}

	public void setScores(ArrayList<Integer> scores) {
		this.scores = scores;
	}
}
