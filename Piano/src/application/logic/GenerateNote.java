package application.logic;

import java.util.ArrayList;
import java.util.Random;

public class GenerateNote {
	
	private static Random random = new Random();
	
	private static String[] notes = {
		"C4", "C4#", "D4", "D4#", "E4", "F4", 
		"F4#", "G4", "G4#", "A4", "A4#", "B4", 
		"C5", "C5#", "D5", "D5#", "E5"
	};
	
	public static String randomNote() {		
		int index = random.nextInt(notes.length/* + 5*/);
		return index >= notes.length 
			? ""
			: notes[index];
	}
	
	public static ArrayList<String> getSequence(int length) {
		
		var sequence = new ArrayList<String>(length);
		for(int i = 0; i < length ;i++) {
			sequence.add(randomNote());
		}
		return sequence;
	}
	
	
}
