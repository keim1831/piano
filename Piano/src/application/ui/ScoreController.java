package application.ui;

import java.io.IOException;

import application.logic.IMainViewController;
import application.logic.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class ScoreController implements IMainViewController {
	private Router router;
	
	@Override
	public void setRouter(Router router) {
		this.router = router;
	}
	
	@Override
	public void setState(State state) {
	}
	
	@FXML
	private void backToMenu(ActionEvent event) throws IOException {
		router.toMenu();
	}
}
