package application.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import application.logic.IMainViewController;
import application.logic.NotePlayer;
import application.logic.State;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class PlayerPianoController implements IMainViewController, INotePlayedHandler {
	private Router router;
	private NotePlayer notePlayer = new NotePlayer();
	
	
	@FXML 
	private AnchorPane pianoKeyboard;
	
	@FXML 
	private PianoKeyboardController pianoKeyboardController;
	
	@FXML private ChoiceBox<String> songChoiceBox;
	
	@Override
	public void setRouter(Router router) {
		this.router = router;
	}
	
	@Override
	public void setState(State state) {
	}
	
	
	@FXML
	protected void initialize() throws Exception {
				

		Stream<Path> walk = Files.walk(Paths.get("resources\\songs"));
		List<String> result = walk.filter(Files::isRegularFile)
				.map(x -> getFileNameWithoutExtension(x)).collect(Collectors.toList());
		walk.close();
		
		var choices = FXCollections.observableArrayList(result);
		songChoiceBox.setItems(choices);
		songChoiceBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
	      @Override
	      public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	        try {
	        	startPlayingSong(newValue);
			} catch (Exception e) {
			}
	      }
	    });
	}
	
	private String getFileNameWithoutExtension(Path path) {
		var name = path.getFileName().toString();
		return name.substring(0, name.length()-4);
	}
	
	private void startPlayingSong(String path) throws Exception {
		var t = new Thread() {
			@Override
			public void run() {
				try {
					System.out.println("will start playing: " + path);
					
					var notes = new ArrayList<String>();
					notes.add("");
					
					File file = new File("resources\\songs\\" + path + ".txt");  
					BufferedReader br = new BufferedReader(new FileReader(file)); 
					String st; 
					while ((st = br.readLine()) != null) {
						notes.add(st.trim());
				  	} 
					
					br.close();
					
					notes.add("");
					
					var listIterator = notes.iterator();
					
					var customIterator = new Iterator<String>() {
						@Override
						public boolean hasNext() {
							return listIterator.hasNext();
						}
	
						@Override
						public String next() {
							var key = listIterator.next();
							pianoKeyboardController.clearHighLight();
							if (key.length() > 0)
								pianoKeyboardController.setHighlight(key);
							return key;
						}
					};
					
					notePlayer.playSequence(customIterator);
				}
				catch (Exception e) {}
			}
		};
		
		t.start();
	}
	
	@FXML
	private void backToMenu(ActionEvent event) throws IOException {
		notePlayer.stopSequence();
		router.toMenu();
	}
	
	@FXML
	private void notePressed(MouseEvent event) {
		var source = event.getSource();
		
		if (source instanceof Button) {
			var button = (Button) event.getTarget();
			var key = button.getText();
			notePlayer.playSingle(key);
		}
	}

	@FXML
	private void replayPressed(ActionEvent event) throws Exception {
		notePlayer.stopSequence();
		startPlayingSong(songChoiceBox.getSelectionModel().getSelectedItem());
	}
	
	@FXML
	private void stopPressed(ActionEvent event) {
		notePlayer.stopSequence();
	}
	
	@Override
	public void notePlayed(String key) {
		if (notePlayer.isPlaying())
			return;
		
		notePlayer.playSingle(key);
	}
}

