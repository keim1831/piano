package application.ui;

import java.io.IOException;

import application.logic.IMainViewController;
import application.logic.NotePlayer;
import application.logic.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class PianoController implements IMainViewController, INotePlayedHandler {
	private Router router;
	private NotePlayer notePlayer = new NotePlayer();
	
	@FXML 
	private AnchorPane pianoKeyboard;
	
	@FXML 
	private PianoKeyboardController pianoKeyboardController;
	
	@Override
	public void setRouter(Router router) {
		this.router = router;
	}
	
	@Override
	public void setState(State state) {
	}
	
	@FXML
	protected void initialize() {
		pianoKeyboardController.setHandler(this);
	}
	
	@FXML
	private void backToMenu(ActionEvent event) throws IOException {
		router.toMenu();
	}
	
	@FXML
	private void notePressed(MouseEvent event) {
		var source = event.getSource();
		
		if (source instanceof Button) {
			var button = (Button) event.getTarget();
			var key = button.getText();
			notePlayer.playSingle(key);
		}
	}

	@Override
	public void notePlayed(String key) {
		notePlayer.playSingle(key);
	}
}

