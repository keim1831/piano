package application.ui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

public class PianoKeyboardController {
	
	private INotePlayedHandler handler;
	//most
	private final String styleWhite = 
			"-fx-background-color: white; -fx-border-color: black; -fx-border-width: 0.1;";
	private final String styleWhiteSelected = 
			"-fx-background-color: #FF0; -fx-border-color: black; -fx-border-width: 0.1;";
	private final String styleBlack = "-fx-background-color: black;";
	private final String styleBlackSelected = "-fx-background-color: #FF0;";
	//eddig
	
	public void setHandler(INotePlayedHandler handler) {
		this.handler = handler;
	}
	
	public void setHighlight(String key) {
		if (butC4 == null) return;
		/*
		switch (key) {
			case "C4": { butC4.setStyle("-fx-background-color: #FF0"); break; }
			case "C4#": { butC4s.setStyle("-fx-background-color: #FF0"); break; }
			case "D4": { butD4.setStyle("-fx-background-color: #FF0"); break; }
			case "D4#": { butD4s.setStyle("-fx-background-color: #FF0"); break; }
			case "E4": { butE4.setStyle("-fx-background-color: #FF0"); break; }
			case "F4": { butF4.setStyle("-fx-background-color: #FF0"); break; }
			case "F4#": { butF4s.setStyle("-fx-background-color: #FF0"); break; }
			case "G4": { butG4.setStyle("-fx-background-color: #FF0"); break; }
			case "G4#": { butG4s.setStyle("-fx-background-color: #FF0"); break; }
			case "A4": { butA4.setStyle("-fx-background-color: #FF0"); break; }
			case "A4#": { butA4s.setStyle("-fx-background-color: #FF0"); break; }
			case "B4": { butB4.setStyle("-fx-background-color: #FF0"); break; }
			case "C5": { butC5.setStyle("-fx-background-color: #FF0"); break; }
			case "C5#": { butC5s.setStyle("-fx-background-color: #FF0"); break; }
			case "D5": { butD5.setStyle("-fx-background-color: #FF0"); break; }
			case "D5#": { butD5s.setStyle("-fx-background-color: #FF0"); break; }
			case "E5": { butE5.setStyle("-fx-background-color: #FF0"); }
		}
		*/
		switch (key) {
		case "C4": { butC4.setStyle(styleWhiteSelected); break; }
		case "C4#": { butC4s.setStyle(styleBlackSelected); break; }
		case "D4": { butD4.setStyle(styleWhiteSelected); break; }
		case "D4#": { butD4s.setStyle(styleBlackSelected); break; }
		case "E4": { butE4.setStyle(styleWhiteSelected); break; }
		case "F4": { butF4.setStyle(styleWhiteSelected); break; }
		case "F4#": { butF4s.setStyle(styleBlackSelected); break; }
		case "G4": { butG4.setStyle(styleWhiteSelected); break; }
		case "G4#": { butG4s.setStyle(styleBlackSelected); break; }
		case "A4": { butA4.setStyle(styleWhiteSelected); break; }
		case "A4#": { butA4s.setStyle(styleBlackSelected); break; }
		case "B4": { butB4.setStyle(styleWhiteSelected); break; }
		case "C5": { butC5.setStyle(styleWhiteSelected); break; }
		case "C5#": { butC5s.setStyle(styleBlackSelected); break; }
		case "D5": { butD5.setStyle(styleWhiteSelected); break; }
		case "D5#": { butD5s.setStyle(styleBlackSelected); break; }
		case "E5": { butE5.setStyle(styleWhiteSelected); }
	}
		
	}
	
	public void clearHighLight() {
		if (butC4 == null) return;
		/*
		butC4.setStyle("-fx-background-color: #FFF");
		butC4s.setStyle("-fx-background-color: #000");
		butD4.setStyle("-fx-background-color: #FFF");
		butD4s.setStyle("-fx-background-color: #000");
		butE4.setStyle("-fx-background-color: #FFF");
		butF4.setStyle("-fx-background-color: #FFF");
		butF4s.setStyle("-fx-background-color: #000");
		butG4.setStyle("-fx-background-color: #FFF");
		butG4s.setStyle("-fx-background-color: #000");
		butA4.setStyle("-fx-background-color: #FFF");
		butA4s.setStyle("-fx-background-color: #000");
		butB4.setStyle("-fx-background-color: #FFF");
		butC5.setStyle("-fx-background-color: #FFF");
		butC5s.setStyle("-fx-background-color: #000");
		butD5.setStyle("-fx-background-color: #FFF");
		butD5s.setStyle("-fx-background-color: #000");
		butE5.setStyle("-fx-background-color: #FFF");
		*/
		
		var x = butC4s.getStyle();
		butC4.setStyle(styleWhite);
		butC4s.setStyle(styleBlack);
		butD4.setStyle(styleWhite);
		butD4s.setStyle(styleBlack);
		butE4.setStyle(styleWhite);
		butF4.setStyle(styleWhite);
		butF4s.setStyle(styleBlack);
		butG4.setStyle(styleWhite);
		butG4s.setStyle(styleBlack);
		butA4.setStyle(styleWhite);
		butA4s.setStyle(styleBlack);
		butB4.setStyle(styleWhite);
		butC5.setStyle(styleWhite);
		butC5s.setStyle(styleBlack);
		butD5.setStyle(styleWhite);
		butD5s.setStyle(styleBlack);
		butE5.setStyle(styleWhite);
	}
	
	@FXML private Button butC4;
	@FXML private Button butC4s;
	@FXML private Button butD4;
	@FXML private Button butD4s;
	@FXML private Button butE4;
	@FXML private Button butF4;
	@FXML private Button butF4s;
	@FXML private Button butG4;
	@FXML private Button butG4s;
	@FXML private Button butA4;
	@FXML private Button butA4s;
	@FXML private Button butB4;
	@FXML private Button butC5;
	@FXML private Button butC5s;
	@FXML private Button butD5;
	@FXML private Button butD5s;
	@FXML private Button butE5;
	
	@FXML
	private void c4Pressed(MouseEvent event) {
		handler.notePlayed("C4");
	}
	
	@FXML
	private void c4sPressed(MouseEvent event) {
		handler.notePlayed("C4#");
	}
	
	@FXML
	private void d4Pressed(MouseEvent event) {
		handler.notePlayed("D4");
	}
	
	@FXML
	private void d4sPressed(MouseEvent event) {
		handler.notePlayed("D4#");
	}
	
	@FXML
	private void e4Pressed(MouseEvent event) {
		handler.notePlayed("E4");
	}
	
	@FXML
	private void f4Pressed(MouseEvent event) {
		handler.notePlayed("F4");
	}
	
	@FXML
	private void f4sPressed(MouseEvent event) {
		handler.notePlayed("F4#");
	}
	
	@FXML
	private void g4Pressed(MouseEvent event) {
		handler.notePlayed("G4");
	}
	
	@FXML
	private void g4sPressed(MouseEvent event) {
		handler.notePlayed("G4#");
	}
	
	@FXML
	private void a4Pressed(MouseEvent event) {
		handler.notePlayed("A4");
	}
	
	@FXML
	private void a4sPressed(MouseEvent event) {
		handler.notePlayed("A4#");
	}
	
	@FXML
	private void b4Pressed(MouseEvent event) {
		handler.notePlayed("B4");
	}
	
	@FXML
	private void c5Pressed(MouseEvent event) {
		handler.notePlayed("C5");
	}
	
	@FXML
	private void c5sPressed(MouseEvent event) {
		handler.notePlayed("C5#");
	}
	
	@FXML
	private void d5Pressed(MouseEvent event) {
		handler.notePlayed("D5");
	}
	
	@FXML
	private void d5sPressed(MouseEvent event) {
		handler.notePlayed("D5#");
	}
	
	@FXML
	private void e5Pressed(MouseEvent event) {
		handler.notePlayed("E5");
	}
}

