package application.ui;

import java.io.IOException;

import application.logic.IMainViewController;
import application.logic.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class MenuController implements IMainViewController {
	private Router router;
	
	@Override
	public void setRouter(Router router) {
		this.router = router;
	}
	
	@Override
	public void setState(State state) {
	}
	
	@FXML
	private void openPiano(ActionEvent event) throws IOException{
		router.toPiano();
	}
	
	@FXML
	private void openPlayerPiano(ActionEvent event) throws IOException{
		router.toPlayerPiano();
	}
	
	@FXML
	private void openPianoMemoryGame(ActionEvent event) throws IOException{
		router.toPianoMemoryGame();
	}
	
	@FXML
	private void openScore(ActionEvent event) throws IOException{
		router.toScore();
	}
	
	@FXML
	private void exitButton(ActionEvent event) throws IOException{
		router.exit();
	}
}
