package application.ui;

import java.io.IOException;

import application.logic.IMainViewController;
import application.logic.State;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Router {
	private Stage stage;
	private State state;
	
	public Router(Stage stage, State state) {
		this.stage = stage;
		this.state = state;
	}
	
	public void toMenu() throws IOException {
		openFxmlAndSetRouter("Menu.fxml");
	}
	
	public void toPiano() throws IOException{
		openFxmlAndSetRouter("Piano.fxml");
	}
	
	public void toPlayerPiano() throws IOException{
		openFxmlAndSetRouter("PlayerPiano.fxml");
	}
	
	public void toPianoMemoryGame() throws IOException{
		openFxmlAndSetRouter("PianoMemoryGame.fxml");
	}
		
	public void toScore() throws IOException{
		openFxmlAndSetRouter("Score.fxml");
	}
	
	public void exit() throws IOException{
		stage.close();
	}
	
	private void openFxmlAndSetRouter(String fileName) throws IOException {
		var url = getClass().getResource("/ui/" + fileName);
		if (url == null)
			url = getClass().getResource(fileName);
			
		var loader = new FXMLLoader();
		loader.setLocation(url);
		Parent view = loader.load(url.openStream());
		Scene scene = new Scene(view);
		
		var controller = (IMainViewController) loader.getController();
		controller.setRouter(this);
		controller.setState(state);
		
		stage.setScene(scene);
	}
}
