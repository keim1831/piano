package application.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import application.logic.GenerateNote;
import application.logic.IMainViewController;
import application.logic.NotePlayer;
import application.logic.State;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class PianoMemoryGameController implements IMainViewController, INotePlayedHandler {
	private Router router;
	private NotePlayer notePlayer = new NotePlayer();
	public boolean ok = false;
	//private boolean endOrNot = true;
	private GenerateNote gn;

	
	public boolean showAlert() {
	    Platform.runLater(new Runnable() {
	      public void run() {
	    		Alert alert = new Alert(Alert.AlertType.NONE);

	    	  	alert.setTitle("Hello in MemoryGame ");
	    		alert.setContentText("Do you want to play? ");
	    		ButtonType okButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
	    		ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);
	    		ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
	    		alert.getButtonTypes().setAll(okButton, noButton, cancelButton);
	    		alert.showAndWait().ifPresent(type -> {
	    		        if (type == ButtonType.YES) {        	
							ok = true;
	    		        } else if (type == ButtonType.NO) {
	    		        	ok = false;
	    		        }
	    		});

	      }
	    });
	    
	    return ok;
	}
	
		
	@FXML 
	private AnchorPane pianoKeyboard;
	
	@FXML 
	private PianoKeyboardController pianoKeyboardController;
	
	@Override
	public void setRouter(Router router) {
		this.router = router;
	}
	
	@Override
	public void setState(State state) {
	}
	
	
	@FXML
	protected void initialize() throws Exception {
		pianoKeyboardController.setHandler(this);
		ArrayList< String> notes = new ArrayList<String>();
		if(showAlert() == true) {
			int i = 1;
			while(i<10) {
	    		notes = gn.getSequence(i);
				startPlayingSong(notes);
	    		i += 3;
			}
		}
	}
	
	
	
	private void startPlayingSong(ArrayList<String> notes) throws Exception {
		var t = new Thread() {
			@Override
			public void run() {
				try {			
					notes.add("");
					
					var listIterator = notes.iterator();
					
					var customIterator = new Iterator<String>() {
						@Override
						public boolean hasNext() {
							return listIterator.hasNext();
						}
	
						@Override
						public String next() {
							var key = listIterator.next();
							pianoKeyboardController.clearHighLight();
							if (key.length() > 0)
								pianoKeyboardController.setHighlight(key);
							return key;
						}
					};
					
					notePlayer.playSequence(customIterator);
				}
				catch (Exception e) {}
			}
		};
		
		t.start();
	}
	
	
	@FXML
	private void backToMenu(ActionEvent event) throws IOException {
		router.toMenu();
	}
	
	@FXML
	private void notePressed(MouseEvent event) {
		var source = event.getSource();
		
		if (source instanceof Button) {
			var button = (Button) event.getTarget();
			var key = button.getText();
			notePlayer.playSingle(key);
		}
	}
	
	

	@Override
	public void notePlayed(String key) {
		notePlayer.playSingle(key);
	}
}

