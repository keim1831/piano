package application;

import java.util.ArrayList;

import application.logic.GenerateNote;
import application.logic.NotePlayer;
import application.logic.State;
import application.ui.Router;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
	public static GenerateNote gn;

	public static void semmi() {
		ArrayList< String> notes = new ArrayList<String>();
		notes = gn.getSequence(5);
		for(String note : notes) {
			System.out.println(note.toString());
		}
	}
	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Pianoo");
			
			var state = new State();
			
			var router = new Router(primaryStage, state);
			router.toMenu();
			
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) throws Exception{
		
		launch(args);
		/*
		var arrlist = NotePlayer.list;
		   System.out.println("For Loop");
		      for (int counter = 0; counter < arrlist.size(); counter++) { 		      
		          System.out.println(arrlist.get(counter)); 		
		      }   		
*/
		//semmi();

	}
}
